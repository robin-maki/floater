const fs = require('fs');
const path = require('path');
const express = require('express');
const exphbs = require('express-handlebars');
const http = require('http');
const app = require('./server');
const socket = require('./socket');

const hbs = exphbs.create({
    helpers: {
        dateFormat(d) {
            d = new Date(d);
            return `${d.getFullYear()}-${d.getMonth()}-${d.getDate()} ${d.getHours()}:${d.getMinutes()}`;
        },
        dateFromObjectId(o) {
            o = new Date(parseInt(o.toString().substring(0, 8), 16) * 1000);
            return `${o.getFullYear()}-${o.getMonth()}-${o.getDate()} ${o.getHours()}:${o.getMinutes()}`;
        }
    }
});
app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');
app.set('views', path.join(__dirname, 'views'));
app.set('trust proxy', true);

fs.readdirSync(path.resolve(__dirname, 'routes')).forEach((name) => {
    require(path.resolve(__dirname, 'routes', name));
});

let server = http.createServer(app);
socket.init(server);


server.listen(3429, () => {
    console.log('Listening on port 3429');
});
