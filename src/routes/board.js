const app = require('../server');
const db = require('../db');
const hash = require('../hash');

app.get('/', async(req, res) => {
    res.render('boardlist', {boards: await db.get('board').find()});
});

app.get('/board/:bid', async(req, res) => {
    let board = await db.get('board').findOne({name: req.params.bid});
    if(!board) {
        res.status(404).json({error: 'Board not found'});
    }
    let threads = await db.get('thread').find({board: board._id}, {sort: '-lastUpdated', limit: 50});
    res.render('threadslist', {board, threads});
});
app.post('/board/:bid', async(req, res) => {
    if(!req.body.title || !req.body.content) {
        res.status(400).send('제목이나 내용을 써주세요.');
        return;
    }
    let board = await db.get('board').findOne({name:req.params.bid});
    if (!board) {
        res.status(404).send('그런 판은 우리에게 있을 수 없어.');
    }
    let thread = await db.get('thread').insert({board: board._id, title: req.body.title, content: req.body.content, lastUpdated: Date.now(), count: 1});
    await db.get('thread').update(thread._id, {$set: {writer: hash(thread._id, req.get('CF-Connecting-IP') || req.ip)}});
    res.redirect('/thread/' + thread._id);
});
