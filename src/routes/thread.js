const app = require('../server');
const db = require('../db');

app.get('/thread/:tid', async(req, res) => {
    let thread = await db.get('thread').findOne(req.params.tid);
    if(!thread) {
        res.status(404).send('그런 스레드는 없습니다.');
    }
    res.render('thread', {thread});
});