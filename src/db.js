const config = require('./config');
const db = require('monk')(config.db);

module.exports = db;

db.then(async() => {
    await db.get('board').createIndex({name: 1}, {unique: true});
    await db.get('thread').createIndex({board: 1, lastUpdated: -1});
    await db.get('subthread').createIndex({parent: 1});
    await db.get('board').insert({name: 'amumal', title: '아무말판'}).catch(() => {});
});
