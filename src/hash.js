const crypto = require('crypto');

module.exports = function(id, ip) {
    let hash = crypto.createHmac('sha256', `${id}${ip}sytgsalt`);
    return hash.digest('base64').slice(0, 8);
};