# Floater
[바로가기](http://floater.kos.moe)

스레드플로트 기반 익명 커뮤니티입니다.

## 특징
socket.io 기반으로 제작되어 페이지 새로고침 없이 실시간으로 서브스레드가 갱신됩니다.

## 실행
MongoDB를 같이 실행해야 하기 때문에 Docker-compose를 이용하여 실행하는 것을 권장합니다. 리포지토리의 `docker-compose.yml` 파일에 포함되어 있습니다.

```$ docker-compose up -d```

## 빌드

```$ docker build .```

특별히 커스터마이징할 필요가 없다면 직접 빌드하기보다는 공식 이미지(registry.gitlab.com/robin-maki/floater)를 사용하는 것을 권장합니다.

## 라이센스

[CC-BY-SA 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/)